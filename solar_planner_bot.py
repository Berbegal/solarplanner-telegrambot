import os
import csv
import time as t
import datetime as dt
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from telegram_bot import TelegramBot
from apis.solar_api import SolarApi
from apis.esios_api import EsiosApi
from helpers.garbage_collector import GarbageCollector

SOLAR_PARAMS_RANGES = {'latitude': {'max': 90, 'min': -90},
                       'longitude': {'max': 180, 'min': -180},
                       'declination': {'max': 90, 'min': 0},
                       'azimuth': {'max': 180, 'min': -180},
                       'installedPowerKw':  {'max': 1000, 'min': 0}}

SOLAR_SETTINGS_STATES = {0: 'location', 1: 'declination',
                         2: 'azimuth', 3: 'installedPowerKw', 5: 'stop'}

CREATION_LIMIT_DAYS = 3
MODIFICATION_LIMIT_DAYS = 2


class SolarPlannerBot(TelegramBot):
#constructor
    def __init__(self, telegramToken, solarToken, esiosToken, chatId, logger=None, console=False):
        super().__init__(telegramToken, chatId, logger, console)
        self._add_command('getSolarPrediction',
                          'to check the solar prediction in kW for today')
        self._add_command('getElectricityPrices',
                          'to check the electricity prices for today')
        self._add_command('getCurrentHour',
                          'to check the solar production estimation and electricity for current hour')
        self._add_command('getDailyReport',
                          'to get a summary of the energy prices and estimated solar production for today')
        self._add_command('checkSolarSettings',
                          'to check current solar parameters such as latitude, longitude, installed power...')
        self._add_command('changeSolarSettings', 'to change solar parameters')
        self._solarApi = SolarApi(solarToken, logger, console)
        self._solarParams = {'latitude': 40.524572441355915, 'longitude': -3.92064795394053,
                             'declination': 21, 'azimuth': -140, 'installedPowerKw': 4.2}
        self._solarParamsInitialized = False
        self._solarPredictionToday = None
        self._esiosApi = EsiosApi(esiosToken, logger, console)
        self._energyPricesToday = None
        self._garbageCollector = GarbageCollector([os.getcwd()+'\\logs', os.getcwd()+'\\output-files'],
                                                  3, 2, True, logger, console)

# helpers
    def _is_float(self, value):
        """Return true if value can be casted to float, false otherwise."""
        try:
            float(value)
            return True
        except ValueError:
            return False

    def _load_from_csv(self, fileName):
        """Return an ok bool and the data loaded from a csv file given by fileName."""
        data = []
        ok = False
        try:
            with open(fileName) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                for row in csv_reader:
                    data.append([row[0], row[1]])
            ok = True
        except Exception as e:
            self._log_utils.log_write(f'While trying to open {fileName}: {e}')
        return ok, data

# solar
    def _initialize_solar(self):
        """When starting up make user check solar parameters."""
        if self._solarParamsInitialized:
            return
        self._botApi.send_message(
            self._chatId, 'Please check if your solar parameters are properly initialized.')
        self._print_solar_settings()
        self._botApi.send_message(
            self._chatId, 'If they are correct, please type /yes to continue, else type /changeSolarSettings to change them.')
        while(1):
            lastUpdateExists, update = self._botApi.get_last_update_from_user(
                self._chatId)
            if lastUpdateExists:
                userText = self._botApi.get_message_text(update)
                if userText == '/yes':
                    self._solarParamsInitialized = True
                    self._botApi.send_message(
                        self._chatId, 'Thank you, we are ready to go. To check what I can do type /help.')
                    break
                elif userText == '/changeSolarSettings':
                    self._solarParamsInitialized = True
                    self._solar_state_machine()
                    self._solarPredictionToday = None
                    self._get_solar_pred(forceApiCall=True)
                    break
                else:
                    self._botApi.send_message(
                        self._chatId, 'Please type /yes to continue with the given solar parameters or type /changeSolarSettings to change them.')
        self._solarPredictionToday = None
        self._get_solar_pred(forceApiCall=True)

    def _print_solar_settings(self):
        """Show current solar settigns."""
        message = 'The current solar settings are:\n'
        for name, value in self._solarParams.items():
            message += f'\t-{name} : {value}\n'
        self._botApi.send_message(self._chatId, message)

    def _solar_state_machine(self):
        """Go through the process of setting new solar parameters."""
        state = 0
        firstMessage = True
        firstMessageDict = ['First, please attach new location:',
                            'Now, please indicate the plane declination (0 degrees horizontal 90 degrees vertical):',
                            'Now, please indicate azimuth (0 degrees = North, 90 degrees= East, 180 degrees = South, -90 degrees = West):',
                            'Lastly, please indicate the installed power in kW:']

        while(1):
            if state == 0:
                if firstMessage:
                    message = firstMessageDict[state]
                    self._botApi.send_message(self._chatId, message)
                    firstMessage = False
                lastUpdateExists, update = self._botApi.get_last_update_from_user(
                    self._chatId)
                if lastUpdateExists:
                    result, location = self._botApi.get_message_location(
                        update)
                    if result:
                        self._solarParams['latitude'] = location[0]
                        self._solarParams['longitude'] = location[1]
                        message = f'New location saved. \n\tLatitude: {self._solarParams["latitude"]}\n\tLongitude: {self._solarParams["longitude"]}'
                        self._botApi.send_message(self._chatId, message)
                        firstMessage = True
                        state += 1
                    else:
                        userText = self._botApi.get_message_text(update)
                        if userText == '/exit':
                            message = 'Settings configuration stopped'
                            self._botApi.send_message(self._chatId, message)
                            state = 5
                        else:
                            message = 'Please send a location or type /exit to stop modifying settings'
                            self._botApi.send_message(self._chatId, message)
            elif state >= 1 and state <= 3:
                if firstMessage:
                    message = firstMessageDict[state]
                    self._botApi.send_message(self._chatId, message)
                    firstMessage = False
                lastUpdateExists, update = self._botApi.get_last_update_from_user(
                    self._chatId)
                if lastUpdateExists:
                    userText = self._botApi.get_message_text(update)
                    if self._is_float(userText) \
                            and float(userText) <= SOLAR_PARAMS_RANGES[SOLAR_SETTINGS_STATES[state]]['max'] \
                            and float(userText) >= SOLAR_PARAMS_RANGES[SOLAR_SETTINGS_STATES[state]]['min']:

                        self._solarParams[SOLAR_SETTINGS_STATES[state]] = float(
                            userText)
                        message = f'New {SOLAR_SETTINGS_STATES[state]} saved. \n\t{SOLAR_SETTINGS_STATES[state]}: {self._solarParams[SOLAR_SETTINGS_STATES[state]]}'
                        self._botApi.send_message(self._chatId, message)
                        firstMessage = True
                        state += 1
                    elif userText == '/exit':
                        message = 'Settings configuration stopped'
                        self._botApi.send_message(self._chatId, message)
                        state = 5
                    else:
                        message = f'Please send a float between {SOLAR_PARAMS_RANGES[SOLAR_SETTINGS_STATES[state]]["min"]}'
                        message += f' and {SOLAR_PARAMS_RANGES[SOLAR_SETTINGS_STATES[state]]["max"]} or type /exit to stop modifying settings'
                        self._botApi.send_message(self._chatId, message)
            elif state == 5:
                return False
            else:
                message = 'All solar settings saved!'
                self._botApi.send_message(self._chatId, message)
                break

            t.sleep(1)

        return True

    def _get_solar_pred(self, sendFiles=False, forceApiCall=False):
        """Get solar prediction for today with the solar parameters currently saved.
        If not previously asked, use api, else use backed-up data"""
        response = None
        figName = f'output-files/{dt.date.today()}_solar_pred.png'
        fileName = f'output-files/{dt.date.today()}_solar_pred.csv'
        filesExist = os.path.isfile(figName) and os.path.isfile(fileName)
        rawData = []
        getOk = True

        if filesExist and not self._solarPredictionToday:
            ok, rawData = self._load_from_csv(fileName)
            if ok:
                self._solarPredictionToday = rawData

        if not filesExist and self._solarPredictionToday:
            rawData = self._solarPredictionToday
            filesExist = self._generate_output_files(figName, fileName, rawData)

        if not self._solarPredictionToday or forceApiCall:
            ok, response = self._solarApi.get_solar_pred_kw(
                self._solarParams['latitude'],
                self._solarParams['longitude'],
                self._solarParams['declination'],
                self._solarParams['azimuth'],
                self._solarParams['installedPowerKw'])
            if ok:
                power = response['result']['watts']
                rawData = []
                times = []
                values = []
                for datestring in power.keys():
                    dateTimeObject = dt.datetime.strptime(
                        str(datestring), '%Y-%m-%d %H:%M:%S')
                    if dateTimeObject.date() == dt.date.today():
                        times.append(dt.datetime.fromisoformat(
                            dateTimeObject.isoformat()))
                        values.append(power[datestring])

                for key, value in zip(times, values):
                    rawData.append([key.isoformat(), value])

                if rawData:
                    self._solarPredictionToday = rawData 
                    filesExist = self._generate_output_files(figName, fileName, rawData)
                else:
                     getOk = False
            elif response:
                getOk = False
                self._botApi.send_message(self._chatId, response)

        if sendFiles and filesExist and getOk:
            self._botApi.send_image(self._chatId, figName)
            self._botApi.send_document(self._chatId, fileName)

        if not getOk:
            self._solarPredictionToday = None
            if os.path.isfile(figName):
                try:
                    os.remove(figName)
                except Exception as e:
                    self._log_utils.log_write(
                        f'\tException: {e}', 'critical')
            if os.path.isfile(fileName):
                try:
                    os.remove(fileName)
                except Exception as e:
                    self._log_utils.log_write(
                        f'\tException: {e}', 'critical')
            self._botApi.send_message(
                        self._chatId, 'Sorry, something went wrong and I could not retreive the requested solar production data :(. Please try again later')

# prices
    def _get_electricity_prices(self, sendFiles=False):
        """Get electricity prices for today if not previously asked, use api, else use backed-up data."""
        figName = f'output-files/{dt.date.today()}_energy_prices.png'
        fileName = f'output-files/{dt.date.today()}_energy_prices.csv'
        filesExist = os.path.isfile(figName) and os.path.isfile(fileName)
        rawData = []
        getOk = True
        
        if filesExist and not self._energyPricesToday:
            ok, rawData = self._load_from_csv(fileName)
            if ok:
                self._energyPricesToday = rawData

        if not filesExist and self._energyPricesToday:
            rawData = self._energyPricesToday
            filesExist = self._generate_output_files(figName, fileName, rawData)

        if not self._energyPricesToday:
            ok, response = self._esiosApi.get_electricity_prices()
            if ok:
                prices = response['indicator']['values']
                times = []
                values = []
                for row in prices:
                    if row['geo_name'] == 'Península':
                        dateTimeObject = dt.datetime.fromisoformat(
                            row['datetime'])
                        if dateTimeObject.date() == dt.date.today():
                            times.append(dateTimeObject)
                            values.append(row['value'])

                for key, value in zip(times, values):
                    rawData.append([key.isoformat(), value])

                if rawData:
                    self._energyPricesToday = rawData 
                    filesExist = self._generate_output_files(figName, fileName, rawData)
                else:
                     getOk = False
            elif response:
                getOk = False
                self._botApi.send_message(self._chatId, response)

        if sendFiles and filesExist and getOk:
            self._botApi.send_image(self._chatId, figName)
            self._botApi.send_document(self._chatId, fileName)

        if not getOk:
            self._energyPricesToday = None
            if os.path.isfile(figName):
                try:
                    os.remove(figName)
                except Exception as e:
                    self._log_utils.log_write(
                        f'\tException: {e}', 'critical')
            if os.path.isfile(fileName):
                try:
                    os.remove(fileName)
                except Exception as e:
                    self._log_utils.log_write(
                        f'\tException: {e}', 'critical')
            self._botApi.send_message(
                        self._chatId, 'Sorry, something went wrong and I could not retreive the requested electricity prices data :(. Please try again later')

# solar and prices
    def _get_current_values(self):
        """Show estimated solar production and electricity price for current hour."""
        self._get_solar_pred()
        self._get_electricity_prices()
        currentPrice = 0
        currentSolarProduction = 0
        now = dt.datetime.now()

        if self._energyPricesToday:
            for row in self._energyPricesToday:
                if dt.datetime.fromisoformat(row[0]).hour == now.hour:
                    currentPrice = row[1]
                    break

        if self._solarPredictionToday:
            for row in self._solarPredictionToday:
                if dt.datetime.fromisoformat(row[0]).hour == now.hour:
                    currentSolarProduction = row[1]
                    break

        message = f'The current solar production for this hour ({now.hour}:00-{now.hour}:59) is estimated in '
        message += f'{currentSolarProduction} Watts and the current market price for this hour is {currentPrice} €/ MWh'
        self._botApi.send_message(self._chatId, message)

    def _produce_daily_report(self):
        """Produce a detailed report for the day including solar and market data."""
        self._get_solar_pred()
        self._get_electricity_prices()

        energyProductionKwh = 0
        energyProductionMarketValue = 0
        sunriseTime = None
        sunsetTime = None
        peakProductionW = 0
        peakProductionTime = None
        avgPrice = 0
        maxPrice = -999999
        maxPriceHour = 0
        minPrice = 999999
        minPriceHour = 0
        hourPriceDict = {}

        message = 'Daily report:\n'

        if self._energyPricesToday:
            for row in self._energyPricesToday:
                if float(row[1]) > maxPrice:
                    maxPrice = float(row[1])
                    maxPriceHour = dt.datetime.fromisoformat(row[0]).hour
                if float(row[1]) < minPrice:
                    minPrice = float(row[1])
                    minPriceHour = dt.datetime.fromisoformat(row[0]).hour
                hourPriceDict[dt.datetime.fromisoformat(
                    row[0]).hour] = float(row[1])
                avgPrice += float(row[1])/24

            message += f'-Today\'s average energy price is {"{:.2f}".format(avgPrice)} €/ MWh '
            message += f' with a maximum price of {"{:.2f}".format(maxPrice)}'
            message += f' €/ MWh at {maxPriceHour}:00 and a minum price of {"{:.2f}".format(minPrice)} €/ MWh at {minPriceHour}:00.\n'

        if self._solarPredictionToday:
            sunriseTime = dt.datetime.fromisoformat(
                self._solarPredictionToday[0][0])
            sunsetTime = dt.datetime.fromisoformat(
                self._solarPredictionToday[len(self._solarPredictionToday)-1][0])
            for row in self._solarPredictionToday:
                if float(row[1]) > peakProductionW:
                    peakProductionW = float(row[1])
                    peakProductionTime = dt.datetime.fromisoformat(row[0])
                incrementalEnergy = float(
                    row[1])*(60-dt.datetime.fromisoformat(row[0]).minute)/60/1000
                energyProductionKwh += incrementalEnergy
                if self._energyPricesToday:
                    energyProductionMarketValue += incrementalEnergy * \
                        hourPriceDict[dt.datetime.fromisoformat(row[0]).hour]/1000
            # The previous loop will add the last hour as full and the last date as partial. These error must be corrected
            lastValue = self._solarPredictionToday[len(
                self._solarPredictionToday)-1]
            excessEnergy = 2 * \
                float(
                    lastValue[1])*dt.datetime.fromisoformat(lastValue[0]).minute/60/1000
            energyProductionKwh -= excessEnergy
            if self._energyPricesToday:
                energyProductionMarketValue -= excessEnergy * \
                    hourPriceDict[dt.datetime.fromisoformat(
                        lastValue[0]).hour]/1000

            message += f'-Today there will be a total estimated solar production of {"{:.2f}".format(energyProductionKwh)} kWh,'
            message += f' with a market value of {"{:.2f}".format(energyProductionMarketValue)} €.\n'
            message += f'-Sunrise will be at {sunriseTime.hour}:{sunriseTime.minute}'
            message += f' and sunset will be at {sunsetTime.hour}:{sunriseTime.minute}.'
            message += f' Peak production is estimated at {"{:.2f}".format(peakProductionW)} Watts'
            message += f' at {peakProductionTime.hour}:00.\n'

        self._botApi.send_message(self._chatId, message)
    
    def _generate_output_files(self, figName, fileName, rawData):
        """Produce the csv and png files from rawData and save them as fileName and figName"""
        if not rawData:
            return False

        np.savetxt(fileName, np.array(rawData),
                    delimiter=',', fmt='%s')  
        times = []
        values = []
        for pair in rawData:
            times.append(dt.datetime.fromisoformat(pair[0]))
            values.append(float(pair[1]))
        if 'solar' in figName:
            matplotlib.rcParams['timezone'] = 'UTC'
        elif 'prices' in figName:
            matplotlib.rcParams['timezone'] = 'Europe/Madrid'
        timesPlot = mdates.date2num(times)
        fig, ax = plt.subplots()
        ax.plot(timesPlot, values)
        majorFmt = mdates.DateFormatter('%H:%M')
        hourlocator = mdates.HourLocator()
        ax.xaxis.set_major_locator(hourlocator)
        ax.xaxis.set_major_formatter(majorFmt)
        if 'solar' in figName:
            ax.set(xlabel='Time', ylabel='Power (W)',
            title='Solar production estimation')
        elif 'prices' in figName:
            ax.set(xlabel='Time', ylabel='Energy price (€/MWh)',
            title='Energy Prices')
        
        ax.grid()
        fig.autofmt_xdate()
        fig.savefig(figName)
        plt.close()

        return os.path.isfile(figName) and os.path.isfile(fileName)

# virtuals
    def _virtual_check_user_input(self):
        """Read last message from user."""
        last_update_exists, last_update = self._botApi.get_last_update_from_user(
            self._chatId)
        if last_update_exists:
            self._reply_user(self._botApi.get_message_text(last_update))

    def _virtual_reply_user(self, userText):
        """Reply to last user message."""
        if userText == '/getSolarPrediction':
            self._botApi.send_message(
                self._chatId, 'Sure! Retreiving solar prediction data...')
            self._get_solar_pred(sendFiles=True)
        elif userText == '/getElectricityPrices':
            self._botApi.send_message(
                self._chatId, 'Sure! Retreiving electricity market data...')
            self._get_electricity_prices(sendFiles=True)
        elif userText == '/getCurrentHour':
            self._botApi.send_message(
                self._chatId, 'Sure! This is the solar and price estimation for this hour...')
            self._get_current_values()
        elif userText == '/getDailyReport':
            self._produce_daily_report()
        elif userText == '/checkSolarSettings':
            self._print_solar_settings()
        elif userText == '/changeSolarSettings':
            self._print_solar_settings()
            self._solar_state_machine()
            self._solarPredictionToday = None
            self._get_solar_pred(forceApiCall=True)
        else:
            self._reply_user_base(userText)

    def _virtual_task(self):
        """Execute main task of solar bot."""
        self._initialize_solar()
        self._check_user_input()
        now = dt.datetime.now()
        if not self._sleeping:

            if now.day != self._day:
                self._day = now.day
                self._garbageCollector.execute()
                self._solarPredictionToday = None
                self._energyPricesToday = None
                self._produce_daily_report()
                

            if now.hour != self._hour:
                self._hour = now.hour

            quarter_now = int(now.minute/15)+now.hour*4
            if quarter_now != self._quarter:
                self._quarter = quarter_now
