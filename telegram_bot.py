import time as t
import datetime as dt
from helpers.log_utils import LogUtils
from apis.telegram_api import TelegramApi


class TelegramBot:
#constructor
    def __init__(self, token, chatId, logger=None, console=False):
        self._token = token
        self._chatId = chatId
        self._log_utils = LogUtils(logger, console)
        self._initialize(logger, console)

    def _initialize(self, logger, console):
        self._sleeping = False
        self._dailyCheckHour = 6
        self._now = dt.datetime.now()
        self._hour = self._now.hour-1
        self._quarter = int(self._now.minute/15)+self._hour*4
        self._day = self._now.day
        self._month = self._now.month-1
        self._loopFreqSec = 1
        self._commandDict = {}
        self._commandDict['awake'] = 'to activate the regular bot reports'
        self._commandDict['sleep'] = 'to deactivate the regular bot reports'
        self._commandDict['status'] = 'to see if bot is <b>awake</b> or <b>sleeping</b>'
        # self._commandDict['clear'] = 'to introduce a gap in the chat'
        # self._commandDict['tnow'] = 'to get local bot time in string format and unix timestamp'
        # self._commandDict['fromUnix'] = 'to translate a unix tiemstamp to string. Syntax: /fromUnix(timestamp)'
        self._commandDict['help'] = 'to see this list again'

        self._commandListString = ''
        for key, value in self._commandDict.items():
            self._commandListString += f'\t-/{key}: {value}\n'

        try:
            self._botApi = TelegramApi(self._token, logger, console)

            message = 'I was just started up...'
            self._botApi.send_message(self._chatId, message)
        except Exception as e:
            while (1):
                self._botApi = TelegramApi(self._token, logger, console)
                message = 'I was not able to initialize properly, missing config.ini file or incorrect content.'
                self._botApi.send_message(self._chatId, message)
                t.sleep(900)

#virtuals
    def _virtual_check_user_input(self):
        """Function to be implemented in a child class of TelegramBot."""
        raise NotImplementedError()

    def _virtual_reply_user(self, userText):
        """Function to be implemented in a child class of TelegramBot."""
        raise NotImplementedError()

    def _virtual_task(self):
        """Function to be implemented in a child class of TelegramBot."""
        raise NotImplementedError()

#private
    def _add_command(self, command, description):
        """Allow children classes to add new commands to the bot."""
        self._commandDict[command] = description
        self._commandListString += f'\t-/{command}: {description}\n'

    def _check_user_input(self):
        """Get messages to check if user has written something.
        If not called through a child class will raise a NotImplemented error."""
        return self._virtual_check_user_input()

    def _reply_user(self, userText):
        """Answer user messages.
        If not called through a child class will raise a NotImplemented error."""
        return self._virtual_reply_user(userText)

    def _reply_user_base(self, userText):
        """Answer user according to father commands."""
        if userText == '/sleep':
            self._sleeping = True
            message = 'Going to sleep... I won\'t bother you with my reports... zzzz'
            self._botApi.send_message(self._chatId, message)
        elif userText == '/awake':
            self._sleeping = False
            message = 'Waking up! I will send you my daily reports as programmed!'
            self._botApi.send_message(self._chatId, message)
        elif userText == '/status':
            if self._sleeping is False:
                message = 'I am <b>awake</b>! I will send you the programmed reports.\n'
            else:
                message = 'I am <b>sleeping</b>... I will not bother you with my reports.\n'
            self._botApi.send_message(self._chatId, message)
        elif userText == '/clear':
            message = '.\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n.'
            self._botApi.send_message(self._chatId, message)
        elif userText == '/tnow':
            now = dt.datetime.now()
            now_ts = now.timestamp()//1
            message = f'Current local bot time: {dt.datetime.fromtimestamp(now_ts)}\nCurrent UTC time: {dt.datetime.utcfromtimestamp(now_ts)}\nUnix timestamp: {int(now_ts)}'
            self._botApi.send_message(self._chatId, message)
        elif '/fromUnix' in userText:
            ts_str = userText.replace('/fromUnix', '')
            ts_str = ts_str.replace('(', '')
            ts_str = ts_str.replace(')', '')
            if len(ts_str) > 0 and ts_str.isdigit():
                ts = int(ts_str)
                message = f'Unix timestamp {ts} translates to {dt.datetime.utcfromtimestamp(ts)} UTC or {dt.datetime.fromtimestamp(ts)} local bot time'
                self._botApi.send_message(self._chatId, message)
            else:
                message = f'\'{ts_str}\' is not a valid unix timestamp'
                self._botApi.send_message(self._chatId, message)
        elif userText == '/help':
            message = 'I am able to understand the following commands:\n'
            message += self._commandListString
            self._botApi.send_message(self._chatId, message)
        else:
            message = 'Sorry, I can\'t understand you, I am not advanced enough :(\n\n'
            message += 'Try typing /help to see what commands I am able understand\n'
            self._botApi.send_message(self._chatId, message)
        return

#task
    def task(self):
        """Execute main bot task. If not called through a child class will raise a NotImplemented error."""
        return self._virtual_task()
