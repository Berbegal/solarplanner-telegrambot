import time as t
import logging
import configparser
from logging.handlers import TimedRotatingFileHandler
from solar_planner_bot import SolarPlannerBot


if __name__ == "__main__":

    LOOP_FREQUENCY_SECONDS = 1
    CONSOLE_ACTIVE = False
    LOG_LEVEL = logging.INFO

    logging.basicConfig(format='%(asctime)s %(message)s',
                        handlers=[TimedRotatingFileHandler(
                            filename="logs/solarPlannerBot.log",
                            when="midnight",
                            interval=1,
                            backupCount=3)])
    logger = logging.getLogger()
    logger.setLevel(LOG_LEVEL)

    config = configparser.ConfigParser()
    config.read('config/config.ini')
    telegramToken = config['telegram']['token']
    authChatId = config['telegram']['authChatId']
    solarToken = config['solar']['token']
    esiosToken = config['esios']['token']

    solarPlannerBot = SolarPlannerBot(
        telegramToken, solarToken, esiosToken, authChatId, logger, CONSOLE_ACTIVE)

    while True:
        solarPlannerBot.task()
        t.sleep(LOOP_FREQUENCY_SECONDS)
