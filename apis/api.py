from helpers.log_utils import LogUtils


class Api:
    def __init__(self, host, token, logger=None, console=False):
        self._host = host
        self._token = token
        self._baseUrl = ''
        self._headers = ''
        self._log_utils = LogUtils(logger, console)
