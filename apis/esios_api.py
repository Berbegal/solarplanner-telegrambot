import requests
from apis.api import Api

ARCHIVES = {'PVPC_DETALLE_DD': 71,
            'pvpcdesglosehorario': 80,
            'preciovoluntariopconsumidor': 79}

INDICATORS = {'PVPC_suma_componentes': 10391,
              'PVPC': 1001,
              'PVPC_valle': 1005,
              'PVPC_llano': 1004,
              'PVPC_punta': 1003,
              'PVPC_periodos': 1002}

GEO_IDS = {'Peninusla': 8741,
           'Canarias': 8742,
           'Baleares': 8743,
           'Ceuta': 8744,
           'Melilla': 8745}


class EsiosApi(Api):
    def __init__(self, token, logger=None, console=False):
        super().__init__('https://api.esios.ree.es', token, logger, console)
        self._headers = {'Accept': 'application/json; application/vnd.esios-api-v2+json',
                         'Content-Type': 'application/json',
                         'Host': 'api.esios.ree.es',
                         'Authorization': f'Token token={self._token}'}
        self._baseUrl = self._host

    def get_electricity_prices(self):
        url = self._host + '/indicators/' + \
            str(INDICATORS['PVPC_suma_componentes'])
        params = {"geo_ids": GEO_IDS['Peninusla']}
        try:
            response = requests.get(url, headers=self._headers, params=params)

            if not response.ok:
                message = 'Api error: status code: '+response.status_code+' '+response.reason
                self._log_utils.log_write(message, 'error')
                return False, message

            response = response.json()
            return True, response
        except Exception as e:
            self._log_utils.log_write(e, 'critical')
            return False, str(e)

    def get_indicators(self):
        url = 'https://api.esios.ree.es/indicators/'
        try:
            response = requests.get(url, headers=self._headers)

            if not response.ok:
                message = 'Api error: status code: '+response.status_code+' '+response.reason
                self._log_utils.log_write(message, 'error')
                return False, message

            response = response.json()
            return True, response
        except Exception as e:
            self._log_utils.log_write(e, 'critical')
            return False, str(e)

    def get_archives(self):
        url = 'https://api.esios.ree.es/archives'
        try:
            response = requests.get(url, headers=self._headers)

            if not response.ok:
                message = 'Api error: status code: '+str(response.status_code)+' '+str(response.reason)
                self._log_utils.log_write(message, 'error')
                return False, message

            response = response.json()
            return True, response
        except Exception as e:
            self._log_utils.log_write(e, 'critical')
            return False, str(e)
