import requests
from apis.api import Api


class SolarApi(Api):
    def __init__(self, token, logger=None, console=False):
        super().__init__('https://api.forecast.solar', token, logger, console)
        self._baseUrl = self._host+self._token

    def get_solar_pred_kw(self, latitude, longitude, declination, azimuth, installedPower):
        url = self._baseUrl + '/estimate/' + str(latitude) + '/' + str(
            longitude) + '/' + str(declination) + '/' + str(azimuth) + '/'+str(installedPower)
        try:
            response = requests.get(url, headers=self._headers)

            if not response.ok:
                message = 'Api error: status code: '+str(response.status_code)+' '+str(response.reason)
                self._log_utils.log_write(message, 'error')
                return False, message

            response = response.json()
            return True, response
        except Exception as e:
            self._log_utils.log_write(e, 'critical')
            return False, str(e)
