import requests
import time as t
import functools
import multiprocessing.pool
from apis.api import Api


class TelegramApi(Api):
    def __init__(self, token, logger=None, console=False):
        super().__init__('https://api.telegram.org/bot', token, logger, console)
        self._baseUrl = self._host+self._token
        self._prevUpdateId = 0
        self._initialize()

    def _initialize(self):
        try:
            response = requests.get(self._baseUrl + "/getUpdates")
            response = response.json()
            result = response["result"]
            if len(result) == 0:
                return True
            else:
                lastUpdatePos = len(result) - 1
                self._prevUpdateId = self.get_update_id(result[lastUpdatePos])
                return True
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            return False

    def _timeout(max_timeout):
        """Timeout decorator, parameter in seconds."""
        def timeout_decorator(func):
            """Wrap the original function."""
            @functools.wraps(func)
            def func_wrapper(*args, **kwargs):
                """Closure for function."""
                pool = multiprocessing.pool.ThreadPool(processes=1)
                async_result = pool.apply_async(func, args, kwargs)
                # raises a TimeoutError if execution exceeds max_timeout
                returnValue = async_result.get(max_timeout)
                pool.close()
                return returnValue
            return func_wrapper
        return timeout_decorator

   
    @_timeout(30)
    def get_last_update_from_user(self, chatId):
        params = {"offset": self._prevUpdateId+1, "timeout": 1}
        result = []
        try:
            response = requests.get(self._baseUrl + "/getUpdates", data=params)
            response = response.json()
            if "result" not in response:
                return False, ''
            result = response["result"]
            if len(result) == 0:
                return False, result
            else:
                last_update_pos = len(result) - 1
                while last_update_pos != -1 and int(self.get_chat_id(result[last_update_pos])) != int(chatId):
                    last_update_pos -= 1
                self._prevUpdateId = self.get_update_id(
                    result[last_update_pos])
                if int(self.get_chat_id(result[last_update_pos])) != int(chatId):
                    return False, result[last_update_pos]
                else:
                    return True, result[last_update_pos]
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            return False, result

    def send_message(self, chatId, messageText):
        self._log_utils.log_write(messageText)
        params = {"chat_id": chatId, "text": messageText, "parse_mode": "HTML"}
        try:
            response = requests.post(
                self._baseUrl + "/sendMessage", data=params)
            return response.ok
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            self._log_utils.log_write(
                "Sleeping for 5 seconds and trying to send message again...")
            t.sleep(5)
            try:
                response = requests.post(
                    self._baseUrl + "/sendMessage", data=params)
                return response.ok
            except Exception as e:
                self._log_utils.log_write(e, "critical")
                self._log_utils.log_write("Message could not be sent")
                return False

    def send_image(self, chatId, pathToImage):
        try:
            files = {'photo': open(pathToImage, 'rb')}
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            return False
        data = {'chat_id': chatId}
        try:
            response = requests.post(
                self._baseUrl + "/sendPhoto", files=files, data=data)
            return response.ok
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            self._log_utils.log_write(
                "Sleeping for 5 seconds and trying to send image again...")
            t.sleep(5)
            try:
                response = requests.post(
                    self._baseUrl + "/sendPhoto", files=files, data=data)
                return response.ok
            except Exception as e:
                self._log_utils.log_write(e, "critical")
                self._log_utils.log_write("Image could not be sent")
                return False

    def send_document(self, chatId, pathToFile):
        try:
            files = {"chat_id": chatId, "document": open(pathToFile, 'rb')}
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            return False
        data = {'chat_id': chatId}
        try:
            response = requests.post(
                self._baseUrl + "/sendDocument", files=files, data=data)
            return response.ok
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            self._log_utils.log_write(
                "Sleeping for 5 seconds and trying to send message again...")
            t.sleep(5)
            try:
                response = requests.post(
                    self._baseUrl + "/sendDocument", files=files, data=data)
                return response.ok
            except Exception as e:
                self._log_utils.log_write(e, "critical")
                self._log_utils.log_write("Document could not be sent")
                return False

    def get_update_id(self, update):
        try:
            return update["update_id"]
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            return ''

    def get_chat_id(self, update):
        try:
            return update['message']["chat"]["id"]
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            return ''

    def get_user_name(self, update):
        try:
            return update['message']["chat"]["first_name"]
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            return ''

    def get_message_text(self, update):
        try:
            return update["message"]["text"]
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            return ''

    def get_message_gif(self, update):
        try:
            return True, update["message"]["animation"]["file_id"]
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            return False, ''

    def get_message_location(self, update):
        try:
            latitude = update["message"]["location"]["latitude"]
            longitude = update["message"]["location"]["longitude"]
            return True, (latitude, longitude)
        except Exception as e:
            self._log_utils.log_write(e, "critical")
            return False, ''
