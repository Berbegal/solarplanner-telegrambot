import re
import datetime as dt


class LogUtils:
    def __init__(self, logger=None, console=False):
        self._logger = logger
        self._console = console

    def _current_time_string(self):
        return dt.datetime.today().strftime("%m/%d/%Y, %H:%M:%S")

    def _filter_icons(self, string):
        emoji_pattern = re.compile("["
                                   u"\U0001F600-\U0001F64F"  # emoticons
                                   u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                                   u"\U0001F680-\U0001F6FF"  # transport & map symbols
                                   u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                                   u"\U00002702-\U000027B0"
                                   u"\U000024C2-\U0001F251"
                                   "]+", flags=re.UNICODE)
        return emoji_pattern.sub(r'', string)

    def log_write(self, message, level="info"):
        if isinstance(message, str):
            message = self._filter_icons(message)
        if self._console:
            print(f'{self._current_time_string()} <{level}>: {message}')
        if self._logger != None:
            if level == "debug":
                self._logger.debug(message)
            elif level == "info":
                self._logger.info(message)
            elif level == "warning":
                self._logger.warning(message)
            elif level == "error":
                self._logger.error(message)
            elif level == "critical":
                self._logger.critical(message)
            else:
                self._logger.info(message)
        return
