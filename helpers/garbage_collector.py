import os
import datetime as dt
from helpers.log_utils import LogUtils


class GarbageCollector:
    def __init__(self, controlFolders, creationLimit, modificationLimit, checkSubFolders=False, logger=None, console=False):
        self._controlFolders = controlFolders
        self._cLimit = creationLimit
        self._mLimit = modificationLimit
        self._checkSubFolders = checkSubFolders
        self._log_utils = LogUtils(logger, console)

    def _get_list_of_files(self, dirName, recursive):
        """Get all files of ona directory. If recursive==True get also all files in all subdirectories."""
        listOfFile = os.listdir(dirName)
        allFiles = list()

        for entry in listOfFile:
            fullPath = os.path.join(dirName, entry)
            if os.path.isdir(fullPath) and recursive:
                allFiles = allFiles + \
                    self._get_list_of_files(fullPath, recursive)
            elif not os.path.isdir(fullPath):
                allFiles.append(fullPath)

        return allFiles

    def execute(self):
        """Search in control folders for old files to delete. It will look for .csv, .png and .log files that meet the modification or creation criteria."""
        self._log_utils.log_write('Searching for old files in folders...')

        for folder in self._controlFolders:
            self._log_utils.log_write(f'\tLooking into {folder}')

            file_list = self._get_list_of_files(folder, self._checkSubFolders)

            for file in file_list:
                now_time = dt.datetime.today()
                creation_time = dt.datetime.fromtimestamp(
                    os.path.getctime(file))
                last_modification_time = dt.datetime.fromtimestamp(
                    os.path.getmtime(file))
                deltatime1 = (now_time-creation_time)
                deltatime2 = (now_time-last_modification_time)
                # isOld=deltatime1>dt.timedelta(days=self._cLimit) or deltatime2>dt.timedelta(days=self._mLimit)
                isOld = deltatime2 > dt.timedelta(days=self._mLimit)
                isCsv = file.endswith('.csv')
                isLog = file.endswith('.log')
                isImage = file.endswith('.png')

                if (isCsv or isLog or isImage) and isOld:
                    self._log_utils.log_write(f'\t\tRemoving {file}')
                    try:
                        os.remove(file)
                    except Exception as e:
                        self._log_utils.log_write(
                            f'\tException: {e}', 'critical')
